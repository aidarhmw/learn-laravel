<?php 
include ('Animal.php');
include ('Ape.php');
include ('Frog.php');

$sheep = new Animal("shaun");

echo "name :" . $sheep->name; // "shaun"
echo "<br>";
echo "legs :" . $sheep->legs; // 2
echo "<br>";

echo "cold blooded : ";
echo ($sheep->cold_blooded == false) ? "no" : "yes";  // false

echo "<br>";
echo "<br>";
$sungokong = new Ape("kera sakti");
echo "name : " . $sungokong->name; // "kera sakt"
echo "<br>";
echo "legs : " . $sungokong->legs; // 2
echo "<br>";
echo "yell : ".$sungokong->yell(); // "Auooo"
echo "<br>";

echo "<br>";
echo "<br>";
$kodok = new Frog("buduk");
echo "name : " . $kodok->name; // "kera sakt"
echo "<br>";
echo "legs : " . $kodok->legs; // 2
echo "<br>";
echo "jump : ".$kodok->jump(); // "hop hop"
echo "<br>";
?>

