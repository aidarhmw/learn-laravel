<?php 
require_once('Animal.php');

class Frog extends Animal {
    
    public $legs = 4;
    public $cold_blooded = false;

    public function jump() {
        return "hop hop";
    }
}

?>